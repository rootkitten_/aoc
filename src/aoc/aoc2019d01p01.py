import sys

from .common import read_lines


def calc_fuel(mass: int) -> int:
    return (mass // 3) - 2


def main() -> None:
    args = sys.argv

    if len(args) != 2:
        print(
            'Exactly one argument specifying the path to the input file is '
            'required!'
        )

        return

    path = args[1]
    fuel = (calc_fuel(int(mass)) for mass in read_lines(path))
    total = sum(fuel)

    print(f'The total sum of fuel requirements is {total}.')


if __name__ == '__main__':
    main()
