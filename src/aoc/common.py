from typing import Iterable


def read_lines(path: str, encoding: str = 'utf-8') -> Iterable[str]:
    with open(path, 'r', encoding=encoding) as stream:
        for line in stream:
            yield line
