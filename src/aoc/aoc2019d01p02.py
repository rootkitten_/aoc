import sys

from .common import read_lines


def calc_fuel(mass: int) -> int:
    return (mass // 3) - 2


def calc_total_fuel(mass: int) -> int:
    last = mass
    total = 0

    while True:
        last = calc_fuel(last)

        if last <= 0:
            break

        total += last

    return total


def main() -> None:
    args = sys.argv

    if len(args) != 2:
        print(
            'Exactly one argument specifying the path to the input file is '
            'required!'
        )

        return

    path = args[1]
    fuel = (calc_total_fuel(int(mass)) for mass in read_lines(path))
    total = sum(fuel)

    print(f'The total sum of fuel requirements is {total}.')


if __name__ == '__main__':
    main()
